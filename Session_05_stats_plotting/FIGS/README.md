# NumericalAnalysis

Import data sets (check for NAN) 
NumPy arrays to analyze data: slicing, stats (mean, std, ...), calculations on arrays
Using functions
Fitting data: R test, residuals
Plotting with matplotlib, histograms, 2D heatmaps