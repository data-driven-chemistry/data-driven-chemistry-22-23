"""
Functions to read and count NMR peaks.
"""


import pandas as pd
from scipy.signal import find_peaks



def read_NMR_data(NMR):
    """ Read NMR data. """

    data = pd.read_csv(NMR, sep='\t', names=['shift','intensity','derivative'])
    return data


def count_peaks(NMR_data, ID, prominence = 0.01):
    """ Return the number of peaks in an NMR spectrum. 
    
    Takes a dictionary of DataFrames as input
    """
    
    peaks, peak_info = find_peaks(NMR_data[ID]['intensity'],
                                  prominence = prominence,
                                  )

    return len(peaks)


def summary_stats(NMR_dataframe):
    """ Return summary statistics for an NMR dataframe. """
    
    chem_shift_min = NMR_dataframe['shift'].min()
    chem_shift_max = NMR_dataframe['shift'].max()
    shift_range = chem_shift_max - chem_shift_min

    max_intensity = NMR_dataframe['intensity'].max()

    baseline = NMR_dataframe.nsmallest(n = int(0.4*NMR_dataframe.shape[0]), columns=['intensity'])['intensity'].std()
    
    return shift_range, max_intensity, baseline