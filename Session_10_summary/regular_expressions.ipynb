{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Introduction to regular expressions\n",
    "\n",
    "Regular expressions (or 'regexes') are a powerful (but complicated) way to extract information from strings, and they are available in many programming languages. We have used them throughout this course, but often without explanation. Here I'll give a brief introduction, but more details can be found [here](https://docs.python.org/3/howto/regex.html)\n",
    "\n",
    "In Python, you can use the `re` package to use regular expressions. \n",
    "Often you want to find only part of a string that matches, in which case you can just use those characters:\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 41,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['Co']"
      ]
     },
     "execution_count": 41,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "import re\n",
    "\n",
    "my_long_string = \"\"\"Chemistry often uses letters and symbols in a specific way,\n",
    "such as the difference between 'Co', 'CO', 'C0' and 'C-O' or 'C=O' in a formula\"\"\"\n",
    "\n",
    "re.findall('Co', my_long_string)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Special characters\n",
    "Some characters are \"special\" in regexes, however. For instance, square brackets allow you to match any of the characters inside them:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 42,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['Co', 'CO']"
      ]
     },
     "execution_count": 42,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "re.findall('C[oO]', my_long_string)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "You can also use a range of characters between square brackets (roughly speaking alphabetical and numerical order):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 43,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['Ch', 'Co']"
      ]
     },
     "execution_count": 43,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "re.findall('C[a-z]', my_long_string)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "(This now finds anything where a 'C' is followed by a lowercase letter, so also finds the start of **Ch**emistry)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "Going further, the special character \".\" will match **any** single letter or symbol:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 44,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['Ch', 'Co', 'CO', 'C0', 'C-', 'C=']"
      ]
     },
     "execution_count": 44,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "re.findall('C.', my_long_string)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Some characters are defined by a special \"control sequence\", which can also be used in regexes:\n",
    "- `\\t` denotes a tab character\n",
    "- `\\s` matches any single \"whitespace\" character including spaces, tabs or newlines (`\\n`)\n",
    "- `\\d` can match any single decimal digit (and behaves the same as `[0-9]`)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Repeating characters\n",
    "Sometimes we want to make a character optional, or allow it to repeat. Regexes give a number of options for repeating the previous character (or group of characters):\n",
    "- `?` will match the character to the left 0 or 1 times exactly\n",
    "- `+` will match it one or more times\n",
    "- `*` will match it *any* number of time (0 or more)\n",
    "    - You have used `*` with glob to match files with any name (although the behaviour is slightly different)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 52,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['CO', 'C-O', 'C=O']"
      ]
     },
     "execution_count": 52,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# \"C.?O\" will find any C followed by an O, with 0 or 1 additional characters in between:\n",
    "re.findall('C.?O', my_long_string)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> Chemistry often uses letters and symbols in a specific way, such as the difference between 'Co', '<mark>CO</mark>', 'C0' and '<mark>C-O</mark>' or '<mark>C=O</mark>' in a formula"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 57,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['C0']"
      ]
     },
     "execution_count": 57,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# 'C\\d+' will find any C that is followed by one or more digits, so would match both 'C1' and 'C4526', but not 'C'\n",
    "re.findall('C\\d+', my_long_string)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> Chemistry often uses letters and symbols in a specific way, such as the difference between 'Co', 'CO', '<mark>C0</mark>' and 'C-O' or 'C=O' in a formula"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 58,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['Chemistry', 'Co', 'CO', 'C', 'C', 'C']"
      ]
     },
     "execution_count": 58,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# 'C[a-zA-Z]*' will find any C that is followed by other letters, but will stop matching when a non-letter character\n",
    "# (including spaces) is found\n",
    "re.findall('C[a-zA-Z]*', my_long_string)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> <mark>Chemistry</mark> often uses letters and symbols in a specific way, such as the difference between '<mark>Co</mark>', '<mark>CO</mark>', '<mark>C</mark>0' and '<mark>C</mark>-O' or '<mark>C</mark>=O' in a formula"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Groups\n",
    "\n",
    "Occasionally, you might want to match a specific sequence of characters but only return part of that string for further processing; for example, extracting numbers based on their units, but only keeping the value.\n",
    "\n",
    "In this case, you can use round brackets to create a \"group\", which is the only part that is returned. For example, \"Br(\\d)+\" would return the number of digits following Br, so Br2 would return 2 and Br256 would return 256."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 62,
   "metadata": {},
   "outputs": [],
   "source": [
    "methods_text = \"\"\"\n",
    "An 11 mol dm-3 aqeous solution of HNO3 was mixed with a 15 mol dm-3 solution of \n",
    "H2SO4. The resulting mixture was then added dropwise to methylbenzene\n",
    "in a round-bottomed flask with continuous stirring and cooling under ice. \n",
    "0.6 g of off-white solid was recovered using careful filtration.\n",
    "\"\"\"\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 69,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Concentrations in mol dm-3: ['11', '15']\n",
      "Masses in g:  ['0.6']\n"
     ]
    }
   ],
   "source": [
    "print(\"Concentrations in mol dm-3:\", re.findall('(\\d+) mol dm-3', methods_text))\n",
    "\n",
    "# Note the way a decimal point is treated using a literal \".\" character\n",
    "# (using a backslash to escape the special . character, i.e. \"\\.\")\n",
    "print(\"Masses in g: \", re.findall('(\\d+\\.\\d*) g', methods_text))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
